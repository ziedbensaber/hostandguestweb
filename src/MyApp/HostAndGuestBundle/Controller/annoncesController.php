<?php

namespace MyApp\HostAndGuestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use MyApp\HostAndGuestBundle\Entity\Chambre;
use MyApp\HostAndGuestBundle\Entity\Offre;
use MyApp\HostAndGuestBundle\Entity\Addresse;
use Symfony\Component\Validator\Constraints\Count;
use FOS\UserBundle\Model\User;


class annoncesController extends Controller
{
    public function afficherAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $offre = $em->getRepository("MyAppHostAndGuestBundle:Offre")->findAll();

        $images =array();

        foreach($offre as $o)
        {
            $id = $o->getId();
            $adresse = $em->getRepository("MyAppHostAndGuestBundle:Addresse")->find($id);
            $adresse->getRue();
            $o->setAdresse($adresse);

            $chambre = $em->getRepository("MyAppHostAndGuestBundle:Chambre")->findBy(array ('offre'=>$o));

            $o->setNbChambre(count($chambre));
           /* $image = new \Imagick();
            $image->readImageBlob($o->getImage());
            $o->setImage($image);*/

           $images[$o->getId()] = base64_encode(stream_get_contents($o->getImage()));

        }

        $user= $this->get('security.token_storage')->getToken()->getUser();
        //$user1= $this->container->get('security.context')->getToken()->getUser();


        return $this->render('frontend/offre/afficherAnnonces.html.twig', array('offre' => $offre,'adresse' => $adresse,
                                                                                    'id' => $id, 'images' => $images));
    }

    public function afficheDetailAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $offre = $em->getRepository("MyAppHostAndGuestBundle:Offre")->find($id);
        $adresse = $em->getRepository("MyAppHostAndGuestBundle:Addresse")->find($id);
        $chambre = $em->getRepository("MyAppHostAndGuestBundle:Chambre")->findBy(array ('offre'=>$offre));
        $offre->setNbChambre(count($chambre));

        $img = $em->getRepository("MyAppHostAndGuestBundle:OffreImage")->findBy(array ('offre'=>$offre));
        $imageProfil = base64_encode(stream_get_contents($offre->getImage()));
        $images = array();
        foreach ($img as $i){
            $images[$i->getId()] = base64_encode(stream_get_contents($i->getImage()));
        }

        $equipement=$em->getRepository("MyAppHostAndGuestBundle:EquipementLogement")->find($id);
        $espace=$em->getRepository("MyAppHostAndGuestBundle:EspaceLogement")->find($id);
        $reglement=$em->getRepository("MyAppHostAndGuestBundle:Reglement")->find($id);

        return $this->render('frontend/offre/detailAnnonces.html.twig',array('offre' =>$offre,'adresse' =>
                                                                                $adresse,'images'=>$images,'img'=>$img,'imageP'=>$imageProfil,
                                                                                'eq'=>$equipement,'es'=>$espace,'reg'=>$reglement));
    }

    public function homeAction()
    {
        $formFactory = $this->get('fos_user.registration.form.factory');

        $form = $formFactory->createForm();

        return $this->render(':frontend:home.html.twig', array("registration_form" => $form->createView()));
    }

    public function rechercheCritereAction(Request $request)
    {
        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()->getManager();
        /**
         * @var $rep EntityRepository
         */
        $rep = $em->getRepository(Offre::class);

        $debut = \DateTime::createFromFormat('m/d/y',$request->get('debut'));

        $fin = \DateTime::createFromFormat('m/d/y',$request->get('fin'));
        $nbVoyageur = $request->get('nbVoyageur');

        /**
         * @var $offre Offre
         */
        $offre = $rep->findBy(array('dateDebut'=>$debut, 'dateFin'=>$fin, 'nbrVoyageurMax'=>$nbVoyageur));


        $images =array();

        foreach($offre as $o)
        {
            $id = $o->getId();
            $adresse = $em->getRepository("MyAppHostAndGuestBundle:Addresse")->find($id);
            $adresse->getRue();
            $o->setAdresse($adresse);

            $chambre = $em->getRepository("MyAppHostAndGuestBundle:Chambre")->findBy(array ('offre'=>$o));

            $o->setNbChambre(count($chambre));
            $images[$o->getId()] = base64_encode(stream_get_contents($o->getImage()));

        }

        return $this->render('frontend/offre/afficherAnnonces.html.twig', array('offre' => $offre,'adresse' => $adresse,
            'id' => $id, 'images' => $images));


    }


}
