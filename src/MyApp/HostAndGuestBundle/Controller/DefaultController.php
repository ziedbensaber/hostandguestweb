<?php

namespace MyApp\HostAndGuestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $formFactory = $this->get('fos_user.registration.form.factory');

        $form = $formFactory->createForm();

        return $this->render(':frontend:base.html.twig', array("registration_form" => $form->createView()));
    }


    public function backAction()
    {
        return $this->render(':backend:base.html.twig');
    }

}
