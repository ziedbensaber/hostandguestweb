<?php
/**
 * Created by PhpStorm.
 * User: seeker
 * Date: 02/04/2017
 * Time: 22:51
 */

namespace MyApp\HostAndGuestBundle\Controller;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Model\UserManagerInterface;
use MyApp\HostAndGuestBundle\Entity\ReferredHash;
use MyApp\HostAndGuestBundle\Entity\Utilisateur;
use MyApp\HostAndGuestBundle\Form\RegistrationForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Doctrine\DBAL\Driver\PDOStatement;

class ReferController extends Controller
{

    public function generateAction()
    {
//now we have to hide that select box
        $hash = new ReferredHash(new \DateTime(), $this->getUser());

        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()->getManager();

        $em->persist($hash);
        $em->flush();


        return $this->render(":frontend:hash.html.twig", array("hash" => $hash->getHash()));
    }

    public function referAction($hash)
    {

        /**
         * @var $em EntityManager
         */
        $em = $this->getDoctrine()->getManager();
        /**
         * @var $rep EntityRepository
         */
        $rep = $em->getRepository(ReferredHash::class);

        /**
         * @var $hash ReferredHash
         */
        $hash = $rep->findOneBy(array('hash' => $hash));

        $user = new Utilisateur();

        $user->setReferredBy($hash->getUser());

        // adding points

        $id = $user->getReferredBy()->getId();

        $query = "UPDATE `utilisateur` SET `point_fidelite` = `point_fidelite` + '5' WHERE `utilisateur`.`id` = '$id'";
        $this->getDoctrine()->getConnection()->executeUpdate($query);


        $formFactory = $this->get('fos_user.registration.form.factory');


        $form = $this->createForm(RegistrationForm::class, $user);


        return $this->render(':frontend:base.html.twig', array("registration_form" => $form->createView()));

    }

    public function registerAction(Request $request)
    {


        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $form = $this->createForm(RegistrationForm::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            /**
             * @var $em EntityManager
             */
            $em = $this->getDoctrine()->getManager();


            $em->persist($user);
            $em->flush();
            /*
                        $referee = $hash->getUser();
                        $referee->setPointFidelite($referee->getPointFidelite() + 5);
                        var_dump($referee);
                        die;
            */


            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);
            $this->get('session')->set('_security_main', serialize($token));

            return $this->render("@FOSUser/Registration/confirmed.html.twig", array("user" => $user));


        }


        die("form not valid");
    }


}