<?php

namespace MyApp\HostAndGuestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Charite
 *
 * @ORM\Table(name="charite", indexes={@ORM\Index(name="id_guest", columns={"guest_id"})})
 * @ORM\Entity
 */
class Charite
{
    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", nullable=false)
     */
    private $statut;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \MyApp\HostAndGuestBundle\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="MyApp\HostAndGuestBundle\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="guest_id", referencedColumnName="id")
     * })
     */
    private $guest;



    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return Charite
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guest
     *
     * @param \MyApp\HostAndGuestBundle\Entity\Utilisateur $guest
     *
     * @return Charite
     */
    public function setGuest(\MyApp\HostAndGuestBundle\Entity\Utilisateur $guest = null)
    {
        $this->guest = $guest;

        return $this;
    }

    /**
     * Get guest
     *
     * @return \MyApp\HostAndGuestBundle\Entity\Utilisateur
     */
    public function getGuest()
    {
        return $this->guest;
    }
}
