<?php

namespace MyApp\HostAndGuestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Addresse
 *
 * @ORM\Table(name="addresse", indexes={@ORM\Index(name="offre_id", columns={"offre_id"})})
 * @ORM\Entity
 */
class Addresse
{
    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=100, nullable=false)
     */
    private $pays;

    /**
     * @var integer
     *
     * @ORM\Column(name="code_postal", type="integer", nullable=false)
     */
    private $codePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=100, nullable=false)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="gouvernerat", type="string", length=100, nullable=false)
     */
    private $gouvernerat;

    /**
     * @var string
     *
     * @ORM\Column(name="rue", type="string", length=100, nullable=false)
     */
    private $rue;

    /**
     * @var \MyApp\HostAndGuestBundle\Entity\Offre
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="MyApp\HostAndGuestBundle\Entity\Offre")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="offre_id", referencedColumnName="id")
     * })
     */
    private $offre;



    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return Addresse
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set codePostal
     *
     * @param integer $codePostal
     *
     * @return Addresse
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return integer
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Addresse
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set gouvernerat
     *
     * @param string $gouvernerat
     *
     * @return Addresse
     */
    public function setGouvernerat($gouvernerat)
    {
        $this->gouvernerat = $gouvernerat;

        return $this;
    }

    /**
     * Get gouvernerat
     *
     * @return string
     */
    public function getGouvernerat()
    {
        return $this->gouvernerat;
    }

    /**
     * Set rue
     *
     * @param string $rue
     *
     * @return Addresse
     */
    public function setRue($rue)
    {
        $this->rue = $rue;

        return $this;
    }

    /**
     * Get rue
     *
     * @return string
     */
    public function getRue()
    {
        return $this->rue;
    }

    /**
     * Set offre
     *
     * @param \MyApp\HostAndGuestBundle\Entity\Offre $offre
     *
     * @return Addresse
     */
    public function setOffre(\MyApp\HostAndGuestBundle\Entity\Offre $offre)
    {
        $this->offre = $offre;

        return $this;
    }

    /**
     * Get offre
     *
     * @return \MyApp\HostAndGuestBundle\Entity\Offre
     */
    public function getOffre()
    {
        return $this->offre;
    }
}
