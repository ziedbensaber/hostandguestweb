<?php

namespace MyApp\HostAndGuestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @ORM\Table(name="notification", indexes={@ORM\Index(name="emetteur_id", columns={"emetteur_id"}), @ORM\Index(name="recepteur_id", columns={"recepteur_id"})})
 * @ORM\Entity
 */
class Notification
{
    /**
     * @var string
     *
     * @ORM\Column(name="objet", type="string", length=50, nullable=false)
     */
    private $objet;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=100, nullable=false)
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var boolean
     *
     * @ORM\Column(name="vu", type="boolean", nullable=true)
     */
    private $vu = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \MyApp\HostAndGuestBundle\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="MyApp\HostAndGuestBundle\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recepteur_id", referencedColumnName="id")
     * })
     */
    private $recepteur;

    /**
     * @var \MyApp\HostAndGuestBundle\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="MyApp\HostAndGuestBundle\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="emetteur_id", referencedColumnName="id")
     * })
     */
    private $emetteur;



    /**
     * Set objet
     *
     * @param string $objet
     *
     * @return Notification
     */
    public function setObjet($objet)
    {
        $this->objet = $objet;

        return $this;
    }

    /**
     * Get objet
     *
     * @return string
     */
    public function getObjet()
    {
        return $this->objet;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Notification
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Notification
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set vu
     *
     * @param boolean $vu
     *
     * @return Notification
     */
    public function setVu($vu)
    {
        $this->vu = $vu;

        return $this;
    }

    /**
     * Get vu
     *
     * @return boolean
     */
    public function getVu()
    {
        return $this->vu;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recepteur
     *
     * @param \MyApp\HostAndGuestBundle\Entity\Utilisateur $recepteur
     *
     * @return Notification
     */
    public function setRecepteur(\MyApp\HostAndGuestBundle\Entity\Utilisateur $recepteur = null)
    {
        $this->recepteur = $recepteur;

        return $this;
    }

    /**
     * Get recepteur
     *
     * @return \MyApp\HostAndGuestBundle\Entity\Utilisateur
     */
    public function getRecepteur()
    {
        return $this->recepteur;
    }

    /**
     * Set emetteur
     *
     * @param \MyApp\HostAndGuestBundle\Entity\Utilisateur $emetteur
     *
     * @return Notification
     */
    public function setEmetteur(\MyApp\HostAndGuestBundle\Entity\Utilisateur $emetteur = null)
    {
        $this->emetteur = $emetteur;

        return $this;
    }

    /**
     * Get emetteur
     *
     * @return \MyApp\HostAndGuestBundle\Entity\Utilisateur
     */
    public function getEmetteur()
    {
        return $this->emetteur;
    }
}
