<?php

namespace MyApp\HostAndGuestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Paiement
 *
 * @ORM\Table(name="paiement", indexes={@ORM\Index(name="guest_id", columns={"guest_id"}), @ORM\Index(name="reservation_id", columns={"reservation_id"})})
 * @ORM\Entity
 */
class Paiement
{
    /**
     * @var string
     *
     * @ORM\Column(name="method", type="string", nullable=false)
     */
    private $method;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \MyApp\HostAndGuestBundle\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="MyApp\HostAndGuestBundle\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="guest_id", referencedColumnName="id")
     * })
     */
    private $guest;

    /**
     * @var \MyApp\HostAndGuestBundle\Entity\Reservation
     *
     * @ORM\ManyToOne(targetEntity="MyApp\HostAndGuestBundle\Entity\Reservation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="reservation_id", referencedColumnName="id")
     * })
     */
    private $reservation;



    /**
     * Set method
     *
     * @param string $method
     *
     * @return Paiement
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Paiement
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guest
     *
     * @param \MyApp\HostAndGuestBundle\Entity\Utilisateur $guest
     *
     * @return Paiement
     */
    public function setGuest(\MyApp\HostAndGuestBundle\Entity\Utilisateur $guest = null)
    {
        $this->guest = $guest;

        return $this;
    }

    /**
     * Get guest
     *
     * @return \MyApp\HostAndGuestBundle\Entity\Utilisateur
     */
    public function getGuest()
    {
        return $this->guest;
    }

    /**
     * Set reservation
     *
     * @param \MyApp\HostAndGuestBundle\Entity\Reservation $reservation
     *
     * @return Paiement
     */
    public function setReservation(\MyApp\HostAndGuestBundle\Entity\Reservation $reservation = null)
    {
        $this->reservation = $reservation;

        return $this;
    }

    /**
     * Get reservation
     *
     * @return \MyApp\HostAndGuestBundle\Entity\Reservation
     */
    public function getReservation()
    {
        return $this->reservation;
    }
}
