<?php

namespace MyApp\HostAndGuestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EspaceLogement
 *
 * @ORM\Table(name="espace_logement", indexes={@ORM\Index(name="offre_id", columns={"offre_id"})})
 * @ORM\Entity
 */
class EspaceLogement
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="cuisine", type="boolean", nullable=false)
     */
    private $cuisine;

    /**
     * @var boolean
     *
     * @ORM\Column(name="salon", type="boolean", nullable=false)
     */
    private $salon;

    /**
     * @var boolean
     *
     * @ORM\Column(name="lave_linge", type="boolean", nullable=false)
     */
    private $laveLinge;

    /**
     * @var boolean
     *
     * @ORM\Column(name="piscine", type="boolean", nullable=false)
     */
    private $piscine;

    /**
     * @var boolean
     *
     * @ORM\Column(name="jacuzzi", type="boolean", nullable=false)
     */
    private $jacuzzi;

    /**
     * @var boolean
     *
     * @ORM\Column(name="parking", type="boolean", nullable=false)
     */
    private $parking;

    /**
     * @var \MyApp\HostAndGuestBundle\Entity\Offre
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="MyApp\HostAndGuestBundle\Entity\Offre")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="offre_id", referencedColumnName="id")
     * })
     */
    private $offre;



    /**
     * Set cuisine
     *
     * @param boolean $cuisine
     *
     * @return EspaceLogement
     */
    public function setCuisine($cuisine)
    {
        $this->cuisine = $cuisine;

        return $this;
    }

    /**
     * Get cuisine
     *
     * @return boolean
     */
    public function getCuisine()
    {
        return $this->cuisine;
    }

    /**
     * Set salon
     *
     * @param boolean $salon
     *
     * @return EspaceLogement
     */
    public function setSalon($salon)
    {
        $this->salon = $salon;

        return $this;
    }

    /**
     * Get salon
     *
     * @return boolean
     */
    public function getSalon()
    {
        return $this->salon;
    }

    /**
     * Set laveLinge
     *
     * @param boolean $laveLinge
     *
     * @return EspaceLogement
     */
    public function setLaveLinge($laveLinge)
    {
        $this->laveLinge = $laveLinge;

        return $this;
    }

    /**
     * Get laveLinge
     *
     * @return boolean
     */
    public function getLaveLinge()
    {
        return $this->laveLinge;
    }

    /**
     * Set piscine
     *
     * @param boolean $piscine
     *
     * @return EspaceLogement
     */
    public function setPiscine($piscine)
    {
        $this->piscine = $piscine;

        return $this;
    }

    /**
     * Get piscine
     *
     * @return boolean
     */
    public function getPiscine()
    {
        return $this->piscine;
    }

    /**
     * Set jacuzzi
     *
     * @param boolean $jacuzzi
     *
     * @return EspaceLogement
     */
    public function setJacuzzi($jacuzzi)
    {
        $this->jacuzzi = $jacuzzi;

        return $this;
    }

    /**
     * Get jacuzzi
     *
     * @return boolean
     */
    public function getJacuzzi()
    {
        return $this->jacuzzi;
    }

    /**
     * Set parking
     *
     * @param boolean $parking
     *
     * @return EspaceLogement
     */
    public function setParking($parking)
    {
        $this->parking = $parking;

        return $this;
    }

    /**
     * Get parking
     *
     * @return boolean
     */
    public function getParking()
    {
        return $this->parking;
    }

    /**
     * Set offre
     *
     * @param \MyApp\HostAndGuestBundle\Entity\Offre $offre
     *
     * @return EspaceLogement
     */
    public function setOffre(\MyApp\HostAndGuestBundle\Entity\Offre $offre)
    {
        $this->offre = $offre;

        return $this;
    }

    /**
     * Get offre
     *
     * @return \MyApp\HostAndGuestBundle\Entity\Offre
     */
    public function getOffre()
    {
        return $this->offre;
    }
}
