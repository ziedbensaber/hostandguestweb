<?php

namespace MyApp\HostAndGuestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EquipementLogement
 *
 * @ORM\Table(name="equipement_logement", indexes={@ORM\Index(name="offre_id", columns={"offre_id"})})
 * @ORM\Entity
 */
class EquipementLogement
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="wifi", type="boolean", nullable=false)
     */
    private $wifi;

    /**
     * @var boolean
     *
     * @ORM\Column(name="television", type="boolean", nullable=false)
     */
    private $television;

    /**
     * @var boolean
     *
     * @ORM\Column(name="chauffage", type="boolean", nullable=false)
     */
    private $chauffage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cheminee", type="boolean", nullable=false)
     */
    private $cheminee;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dressing", type="boolean", nullable=false)
     */
    private $dressing;

    /**
     * @var boolean
     *
     * @ORM\Column(name="climatisation", type="boolean", nullable=false)
     */
    private $climatisation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="entree_privee", type="boolean", nullable=false)
     */
    private $entreePrivee;

    /**
     * @var boolean
     *
     * @ORM\Column(name="produits", type="boolean", nullable=false)
     */
    private $produits;

    /**
     * @var \MyApp\HostAndGuestBundle\Entity\Offre
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="MyApp\HostAndGuestBundle\Entity\Offre")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="offre_id", referencedColumnName="id")
     * })
     */
    private $offre;



    /**
     * Set wifi
     *
     * @param boolean $wifi
     *
     * @return EquipementLogement
     */
    public function setWifi($wifi)
    {
        $this->wifi = $wifi;

        return $this;
    }

    /**
     * Get wifi
     *
     * @return boolean
     */
    public function getWifi()
    {
        return $this->wifi;
    }

    /**
     * Set television
     *
     * @param boolean $television
     *
     * @return EquipementLogement
     */
    public function setTelevision($television)
    {
        $this->television = $television;

        return $this;
    }

    /**
     * Get television
     *
     * @return boolean
     */
    public function getTelevision()
    {
        return $this->television;
    }

    /**
     * Set chauffage
     *
     * @param boolean $chauffage
     *
     * @return EquipementLogement
     */
    public function setChauffage($chauffage)
    {
        $this->chauffage = $chauffage;

        return $this;
    }

    /**
     * Get chauffage
     *
     * @return boolean
     */
    public function getChauffage()
    {
        return $this->chauffage;
    }

    /**
     * Set cheminee
     *
     * @param boolean $cheminee
     *
     * @return EquipementLogement
     */
    public function setCheminee($cheminee)
    {
        $this->cheminee = $cheminee;

        return $this;
    }

    /**
     * Get cheminee
     *
     * @return boolean
     */
    public function getCheminee()
    {
        return $this->cheminee;
    }

    /**
     * Set dressing
     *
     * @param boolean $dressing
     *
     * @return EquipementLogement
     */
    public function setDressing($dressing)
    {
        $this->dressing = $dressing;

        return $this;
    }

    /**
     * Get dressing
     *
     * @return boolean
     */
    public function getDressing()
    {
        return $this->dressing;
    }

    /**
     * Set climatisation
     *
     * @param boolean $climatisation
     *
     * @return EquipementLogement
     */
    public function setClimatisation($climatisation)
    {
        $this->climatisation = $climatisation;

        return $this;
    }

    /**
     * Get climatisation
     *
     * @return boolean
     */
    public function getClimatisation()
    {
        return $this->climatisation;
    }

    /**
     * Set entreePrivee
     *
     * @param boolean $entreePrivee
     *
     * @return EquipementLogement
     */
    public function setEntreePrivee($entreePrivee)
    {
        $this->entreePrivee = $entreePrivee;

        return $this;
    }

    /**
     * Get entreePrivee
     *
     * @return boolean
     */
    public function getEntreePrivee()
    {
        return $this->entreePrivee;
    }

    /**
     * Set produits
     *
     * @param boolean $produits
     *
     * @return EquipementLogement
     */
    public function setProduits($produits)
    {
        $this->produits = $produits;

        return $this;
    }

    /**
     * Get produits
     *
     * @return boolean
     */
    public function getProduits()
    {
        return $this->produits;
    }

    /**
     * Set offre
     *
     * @param \MyApp\HostAndGuestBundle\Entity\Offre $offre
     *
     * @return EquipementLogement
     */
    public function setOffre(\MyApp\HostAndGuestBundle\Entity\Offre $offre)
    {
        $this->offre = $offre;

        return $this;
    }

    /**
     * Get offre
     *
     * @return \MyApp\HostAndGuestBundle\Entity\Offre
     */
    public function getOffre()
    {
        return $this->offre;
    }
}
