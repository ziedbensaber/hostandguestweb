<?php

namespace MyApp\HostAndGuestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Chambre
 *
 * @ORM\Table(name="chambre", indexes={@ORM\Index(name="offer_id", columns={"offre_id"})})
 * @ORM\Entity
 */
class Chambre
{
    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_lit_double", type="integer", nullable=false)
     */
    private $nbrLitDouble;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_lit_simple", type="integer", nullable=false)
     */
    private $nbrLitSimple;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_lit_bebe", type="integer", nullable=false)
     */
    private $nbrLitBebe;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=100, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \MyApp\HostAndGuestBundle\Entity\Offre
     *
     * @ORM\ManyToOne(targetEntity="MyApp\HostAndGuestBundle\Entity\Offre")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="offre_id", referencedColumnName="id")
     * })
     */
    private $offre;



    /**
     * Set nbrLitDouble
     *
     * @param integer $nbrLitDouble
     *
     * @return Chambre
     */
    public function setNbrLitDouble($nbrLitDouble)
    {
        $this->nbrLitDouble = $nbrLitDouble;

        return $this;
    }

    /**
     * Get nbrLitDouble
     *
     * @return integer
     */
    public function getNbrLitDouble()
    {
        return $this->nbrLitDouble;
    }

    /**
     * Set nbrLitSimple
     *
     * @param integer $nbrLitSimple
     *
     * @return Chambre
     */
    public function setNbrLitSimple($nbrLitSimple)
    {
        $this->nbrLitSimple = $nbrLitSimple;

        return $this;
    }

    /**
     * Get nbrLitSimple
     *
     * @return integer
     */
    public function getNbrLitSimple()
    {
        return $this->nbrLitSimple;
    }

    /**
     * Set nbrLitBebe
     *
     * @param integer $nbrLitBebe
     *
     * @return Chambre
     */
    public function setNbrLitBebe($nbrLitBebe)
    {
        $this->nbrLitBebe = $nbrLitBebe;

        return $this;
    }

    /**
     * Get nbrLitBebe
     *
     * @return integer
     */
    public function getNbrLitBebe()
    {
        return $this->nbrLitBebe;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Chambre
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set offre
     *
     * @param \MyApp\HostAndGuestBundle\Entity\Offre $offre
     *
     * @return Chambre
     */
    public function setOffre(\MyApp\HostAndGuestBundle\Entity\Offre $offre = null)
    {
        $this->offre = $offre;

        return $this;
    }

    /**
     * Get offre
     *
     * @return \MyApp\HostAndGuestBundle\Entity\Offre
     */
    public function getOffre()
    {
        return $this->offre;
    }
}
