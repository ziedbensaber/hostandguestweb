<?php

namespace MyApp\HostAndGuestBundle\Entity;

/**
 * ReferredHash
 *
 * @ORM\Table(name="referred_hash", indexes={@ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity
 */
class ReferredHash
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=32)
     * @ORM\Id
     */
    private $hash;

    /**
     * @var \MyApp\HostAndGuestBundle\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="MyApp\HostAndGuestBundle\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * ReferredHash constructor.
     * @param \DateTime $date
     * @param Utilisateur $user
     */
    public function __construct(\DateTime $date, Utilisateur $user)
    {
        $this->date = $date;
        $this->user = $user;

        $this->hash = md5($this->date->getTimestamp() . $this->user->getId());
    }


    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ReferredHash
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set user
     *
     * @param \MyApp\HostAndGuestBundle\Entity\Utilisateur $user
     *
     * @return ReferredHash
     */
    public function setUser(\MyApp\HostAndGuestBundle\Entity\Utilisateur $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MyApp\HostAndGuestBundle\Entity\Utilisateur
     */
    public function getUser()
    {
        return $this->user;
    }


}
