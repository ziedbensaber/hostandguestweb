<?php

namespace MyApp\HostAndGuestBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Offre
 *
 * @ORM\Table(name="offre", indexes={@ORM\Index(name="host_id", columns={"host_id"})})
 * @ORM\Entity
 */
class Offre
{
    /**
     * @var string
     *
     * @ORM\Column(name="type_logement", type="string", nullable=false)
     */
    private $typeLogement;

    /**
     * @var string
     *
     * @ORM\Column(name="type_location", type="string", nullable=false)
     */
    private $typeLocation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="charite", type="boolean", nullable=true)
     */
    private $charite;

    /**
     * @var boolean
     *
     * @ORM\Column(name="paypal", type="boolean", nullable=true)
     */
    private $paypal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="fidelite", type="boolean", nullable=true)
     */
    private $fidelite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="date", nullable=false)
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="date", nullable=true)
     */
    private $dateFin;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=30, nullable=false)
     */
    private $titre;

    /**
     * @var float
     *
     * @ORM\Column(name="superficie", type="float", precision=10, scale=0, nullable=false)
     */
    private $superficie;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", precision=10, scale=0, nullable=false)
     */
    private $prix;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_voyageur_max", type="integer", nullable=false)
     */
    private $nbrVoyageurMax;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_salle_bain", type="integer", nullable=false)
     */
    private $nbrSalleBain;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=5000, nullable=false)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visibilite", type="boolean", nullable=true)
     */
    private $visibilite = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="blob", length=16777215, nullable=true)
     */
    private $image;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \MyApp\HostAndGuestBundle\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="MyApp\HostAndGuestBundle\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="host_id", referencedColumnName="id")
     * })
     */
    private $host;

    /**
     * @var \MyApp\HostAndGuestBundle\Entity\Addresse
     *

     * @ORM\OneToOne(targetEntity="MyApp\HostAndGuestBundle\Entity\Addresse")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="offre_id", referencedColumnName="adresse")
     * })
     */
    private $adresse;
    /**
     * @ORM\Column(type="integer")
     */
    private $nbChambre=0;

    /**
     * @return Addresse
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param Addresse $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    /**
     * @return mixed
     */
    public function getNbChambre()
    {
        return $this->nbChambre;
    }

    /**
     * @param mixed $nbChambre
     */
    public function setNbChambre($nbChambre)
    {
        $this->nbChambre = $nbChambre;
    }



    /**
     * Set typeLogement
     *
     * @param string $typeLogement
     *
     * @return Offre
     */
    public function setTypeLogement($typeLogement)
    {
        $this->typeLogement = $typeLogement;

        return $this;
    }

    /**
     * Get typeLogement
     *
     * @return string
     */
    public function getTypeLogement()
    {
        return $this->typeLogement;
    }

    /**
     * Set typeLocation
     *
     * @param string $typeLocation
     *
     * @return Offre
     */
    public function setTypeLocation($typeLocation)
    {
        $this->typeLocation = $typeLocation;

        return $this;
    }

    /**
     * Get typeLocation
     *
     * @return string
     */
    public function getTypeLocation()
    {
        return $this->typeLocation;
    }

    /**
     * Set charite
     *
     * @param boolean $charite
     *
     * @return Offre
     */
    public function setCharite($charite)
    {
        $this->charite = $charite;

        return $this;
    }

    /**
     * Get charite
     *
     * @return boolean
     */
    public function getCharite()
    {
        return $this->charite;
    }

    /**
     * Set paypal
     *
     * @param boolean $paypal
     *
     * @return Offre
     */
    public function setPaypal($paypal)
    {
        $this->paypal = $paypal;

        return $this;
    }

    /**
     * Get paypal
     *
     * @return boolean
     */
    public function getPaypal()
    {
        return $this->paypal;
    }

    /**
     * Set fidelite
     *
     * @param boolean $fidelite
     *
     * @return Offre
     */
    public function setFidelite($fidelite)
    {
        $this->fidelite = $fidelite;

        return $this;
    }

    /**
     * Get fidelite
     *
     * @return boolean
     */
    public function getFidelite()
    {
        return $this->fidelite;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     *
     * @return Offre
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return Offre
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Offre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set superficie
     *
     * @param float $superficie
     *
     * @return Offre
     */
    public function setSuperficie($superficie)
    {
        $this->superficie = $superficie;

        return $this;
    }

    /**
     * Get superficie
     *
     * @return float
     */
    public function getSuperficie()
    {
        return $this->superficie;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return Offre
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set nbrVoyageurMax
     *
     * @param integer $nbrVoyageurMax
     *
     * @return Offre
     */
    public function setNbrVoyageurMax($nbrVoyageurMax)
    {
        $this->nbrVoyageurMax = $nbrVoyageurMax;

        return $this;
    }

    /**
     * Get nbrVoyageurMax
     *
     * @return integer
     */
    public function getNbrVoyageurMax()
    {
        return $this->nbrVoyageurMax;
    }

    /**
     * Set nbrSalleBain
     *
     * @param integer $nbrSalleBain
     *
     * @return Offre
     */
    public function setNbrSalleBain($nbrSalleBain)
    {
        $this->nbrSalleBain = $nbrSalleBain;

        return $this;
    }

    /**
     * Get nbrSalleBain
     *
     * @return integer
     */
    public function getNbrSalleBain()
    {
        return $this->nbrSalleBain;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Offre
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set visibilite
     *
     * @param boolean $visibilite
     *
     * @return Offre
     */
    public function setVisibilite($visibilite)
    {
        $this->visibilite = $visibilite;

        return $this;
    }

    /**
     * Get visibilite
     *
     * @return boolean
     */
    public function getVisibilite()
    {
        return $this->visibilite;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Offre
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set host
     *
     * @param \MyApp\HostAndGuestBundle\Entity\Utilisateur $host
     *
     * @return Offre
     */
    public function setHost(\MyApp\HostAndGuestBundle\Entity\Utilisateur $host = null)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get host
     *
     * @return \MyApp\HostAndGuestBundle\Entity\Utilisateur
     */
    public function getHost()
    {
        return $this->host;
    }
}
