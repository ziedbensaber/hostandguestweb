<?php

namespace MyApp\HostAndGuestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservation
 *
 * @ORM\Table(name="reservation", indexes={@ORM\Index(name="guest_id", columns={"guest_id"}), @ORM\Index(name="offer_id", columns={"offer_id"})})
 * @ORM\Entity
 */
class Reservation
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="date", nullable=false)
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="date", nullable=false)
     */
    private $dateFin;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_nuit", type="integer", nullable=false)
     */
    private $nbrNuit;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr_voyageur", type="integer", nullable=false)
     */
    private $nbrVoyageur;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \MyApp\HostAndGuestBundle\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="MyApp\HostAndGuestBundle\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="guest_id", referencedColumnName="id")
     * })
     */
    private $guest;

    /**
     * @var \MyApp\HostAndGuestBundle\Entity\Offre
     *
     * @ORM\ManyToOne(targetEntity="MyApp\HostAndGuestBundle\Entity\Offre")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="offer_id", referencedColumnName="id")
     * })
     */
    private $offer;



    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     *
     * @return Reservation
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return Reservation
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set nbrNuit
     *
     * @param integer $nbrNuit
     *
     * @return Reservation
     */
    public function setNbrNuit($nbrNuit)
    {
        $this->nbrNuit = $nbrNuit;

        return $this;
    }

    /**
     * Get nbrNuit
     *
     * @return integer
     */
    public function getNbrNuit()
    {
        return $this->nbrNuit;
    }

    /**
     * Set nbrVoyageur
     *
     * @param integer $nbrVoyageur
     *
     * @return Reservation
     */
    public function setNbrVoyageur($nbrVoyageur)
    {
        $this->nbrVoyageur = $nbrVoyageur;

        return $this;
    }

    /**
     * Get nbrVoyageur
     *
     * @return integer
     */
    public function getNbrVoyageur()
    {
        return $this->nbrVoyageur;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guest
     *
     * @param \MyApp\HostAndGuestBundle\Entity\Utilisateur $guest
     *
     * @return Reservation
     */
    public function setGuest(\MyApp\HostAndGuestBundle\Entity\Utilisateur $guest = null)
    {
        $this->guest = $guest;

        return $this;
    }

    /**
     * Get guest
     *
     * @return \MyApp\HostAndGuestBundle\Entity\Utilisateur
     */
    public function getGuest()
    {
        return $this->guest;
    }

    /**
     * Set offer
     *
     * @param \MyApp\HostAndGuestBundle\Entity\Offre $offer
     *
     * @return Reservation
     */
    public function setOffer(\MyApp\HostAndGuestBundle\Entity\Offre $offer = null)
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * Get offer
     *
     * @return \MyApp\HostAndGuestBundle\Entity\Offre
     */
    public function getOffer()
    {
        return $this->offer;
    }
}
