<?php

namespace MyApp\HostAndGuestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * Utilisateur
 *
 * @ORM\Table(name="utilisateur", uniqueConstraints={@ORM\UniqueConstraint(name="piece_identite", columns={"piece_identite"})}, indexes={@ORM\Index(name="referred_by", columns={"referred_by"})})
 * @ORM\Entity
 */
class Utilisateur extends BaseUser
{
    /**
     * @var string
     *
     * @ORM\Column(name="piece_identite", type="string", length=30, nullable=true)
     */
    private $pieceIdentite;

    /**
     * @var string
     *
     * @ORM\Column(name="type_identite", type="string", nullable=true)
     */
    private $typeIdentite;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=30, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=30, nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", length=1, nullable=true)
     */
    private $sexe;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=30, nullable=true)
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="adr", type="string", length=100, nullable=true)
     */
    private $adr;


    /**
     * @var integer
     *
     * @ORM\Column(name="point_fidelite", type="integer", nullable=true)
     */
    private $pointFidelite = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=20, nullable=true)
     */
    private $statut = 'Active';

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="blob", nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="interet", type="string", length=20, nullable=true)
     */
    private $interet;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \MyApp\HostAndGuestBundle\Entity\Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="MyApp\HostAndGuestBundle\Entity\Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="referred_by", referencedColumnName="id")
     * })
     */
    protected $referredBy;



    /**
     * Set pieceIdentite
     *
     * @param string $pieceIdentite
     *
     * @return Utilisateur
     */
    public function setPieceIdentite($pieceIdentite)
    {
        $this->pieceIdentite = $pieceIdentite;

        return $this;
    }

    /**
     * Get pieceIdentite
     *
     * @return string
     */
    public function getPieceIdentite()
    {
        return $this->pieceIdentite;
    }

    /**
     * Set typeIdentite
     *
     * @param string $typeIdentite
     *
     * @return Utilisateur
     */
    public function setTypeIdentite($typeIdentite)
    {
        $this->typeIdentite = $typeIdentite;

        return $this;
    }

    /**
     * Get typeIdentite
     *
     * @return string
     */
    public function getTypeIdentite()
    {
        return $this->typeIdentite;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Utilisateur
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Utilisateur
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     *
     * @return Utilisateur
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }


    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return Utilisateur
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set adr
     *
     * @param string $adr
     *
     * @return Utilisateur
     */
    public function setAdr($adr)
    {
        $this->adr = $adr;

        return $this;
    }

    /**
     * Get adr
     *
     * @return string
     */
    public function getAdr()
    {
        return $this->adr;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return Utilisateur
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set pointFidelite
     *
     * @param integer $pointFidelite
     *
     * @return Utilisateur
     */
    public function setPointFidelite($pointFidelite)
    {
        $this->pointFidelite = $pointFidelite;

        return $this;
    }

    /**
     * Get pointFidelite
     *
     * @return integer
     */
    public function getPointFidelite()
    {
        return $this->pointFidelite;
    }

    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return Utilisateur
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Utilisateur
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set interet
     *
     * @param string $interet
     *
     * @return Utilisateur
     */
    public function setInteret($interet)
    {
        $this->interet = $interet;

        return $this;
    }

    /**
     * Get interet
     *
     * @return string
     */
    public function getInteret()
    {
        return $this->interet;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Utilisateur
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set usernameCanonical
     *
     * @param string $usernameCanonical
     *
     * @return Utilisateur
     */
    public function setUsernameCanonical($usernameCanonical)
    {
        $this->usernameCanonical = $usernameCanonical;

        return $this;
    }

    /**
     * Get usernameCanonical
     *
     * @return string
     */
    public function getUsernameCanonical()
    {
        return $this->usernameCanonical;
    }

    /**
     * Set emailCanonical
     *
     * @param string $emailCanonical
     *
     * @return Utilisateur
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->emailCanonical = $emailCanonical;

        return $this;
    }

    /**
     * Get emailCanonical
     *
     * @return string
     */
    public function getEmailCanonical()
    {
        return $this->emailCanonical;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Utilisateur
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Utilisateur
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }



    /**
     * Set confirmationToken
     *
     * @param string $confirmationToken
     *
     * @return Utilisateur
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * Get confirmationToken
     *
     * @return string
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set referredBy
     *
     * @param \MyApp\HostAndGuestBundle\Entity\Utilisateur $referredBy
     *
     * @return Utilisateur
     */
    public function setReferredBy(\MyApp\HostAndGuestBundle\Entity\Utilisateur $referredBy = null)
    {
        $this->referredBy = $referredBy;

        return $this;
    }

    /**
     * Get referredBy
     *
     * @return \MyApp\HostAndGuestBundle\Entity\Utilisateur
     */
    public function getReferredBy()
    {
        return $this->referredBy;
    }
}
